self: super:

let
  overrideOnRACF = pkg: override: if self.config.is_racf or false then (pkg.override override) else pkg;
  overrideAttrsOnRACF = pkg: override: if self.config.is_racf or false then (pkg.overrideAttrs override) else pkg;

in
  {
    pythonOverrides = python-self: python-super: {
      _connection-pool = python-self.callPackage pkgs/connection-pool { };
      _stopit = python-self.callPackage pkgs/stopit { };
      _smart-open = python-self.callPackage pkgs/smart-open { };
      htcondor = python-self.toPythonModule (self.htcondor.override { inherit (python-self) python; });

      # 19.09
      yoda = if python-super ? "yoda" then
        python-super.yoda
      else
        python-self.toPythonModule ((self.yoda.override {
          python2Packages = python-self;
        }).overrideAttrs (old: rec {
          patches = [
          ] ++ self.lib.optionals (self.lib.versionOlder self.yoda.version "1.8.3") [
            # 1.8.3 is an optimistic estimate
            # fixes addBins being too slow
            (self.fetchpatch {
              url = "https://gitlab.com/veprbl/yoda/-/commit/ebc70ed447efe924dd04fc718a6c9e36bbbfddf9.diff";
              sha256 = "1fxh7la0v9wi1m9djzzkg818g8cs2wli64rl1zm5vf2w7x42kz8r";
            })
          ] ++ self.lib.optionals (self.lib.versionOlder self.yoda.version "1.8.1") [
            # fixes "TypeError: expected bytes, str found" in writeYODA()
            (self.fetchpatch {
              url = "https://gitlab.com/hepcedar/yoda/commit/d2bbbe92912457f8a29b440cbfa0b39daf28ec34.diff";
              sha256 = "1x60piswpxwak61r2sdclsc8pzi1fshpkjnxlyflsa1iap77vkq8";
            })
          ];

          nativeBuildInputs = (old.nativeBuildInputs or []) ++ [ python-self.cython ];
          postPatch = (old.postPatch or "") + ''
            touch pyext/yoda/*.{pyx,pxd}
          '';
        }));

      # take too long to test
      numpy = overrideAttrsOnRACF python-super.numpy (_: { doInstallCheck = false; });
      scipy = overrideAttrsOnRACF python-super.scipy (_: { doInstallCheck = false; });
      # fails on test_getrlimit
      cython = overrideAttrsOnRACF python-super.cython (_: { doInstallCheck = false; });
      # python-docker doesn't build
      pandas = overrideAttrsOnRACF python-super.pandas (_: { checkInputs = []; doInstallCheck = false; });
      # test hangs
      ptyprocess = overrideAttrsOnRACF python-super.ptyprocess (_: { doInstallCheck = false; });
      # apparently it needs pytz
      pyarrow = overrideAttrsOnRACF python-super.pyarrow (old: { nativeBuildInputs = old.nativeBuildInputs ++ [ python-self.pytz ]; });

      matplotlib = overrideOnRACF python-super.matplotlib (with self.lib; optionalAttrs (versionAtLeast trivial.release "23.11") { enableTk = false; });
      # too many deps
      awkward = overrideAttrsOnRACF python-super.awkward (_: { doInstallCheck = false; });
      boost_histogram = overrideAttrsOnRACF python-super.boost_histogram (_: { doInstallCheck = false; });
      mplhep = overrideAttrsOnRACF python-super.mplhep (_: { doInstallCheck = false; });
      uproot = overrideAttrsOnRACF python-super.uproot (_: { doInstallCheck = false; });

      # 21.11
      brotli = overrideAttrsOnRACF python-super.brotli (prev: self.lib.optionalAttrs (prev.version == "1.0.9") {
        src = self.fetchFromGitHub {
          owner = "google";
          repo = prev.pname;
          rev = "v${prev.version}";
          sha256 = "sha256-tFnXSXv8t3l3HX6GwWLhEtgpqz0c7Yom5U3k47pWM7o=";
          # for some reason, the test data isn't captured in releases, force a git checkout
          forceFetchGit = true;
        };
      });
      # 21.11
      requests-toolbelt = overrideAttrsOnRACF python-super.requests-toolbelt (_: { doInstallCheck = false; });

      imageio = overrideAttrsOnRACF python-super.imageio (_: { doInstallCheck = false; });

      jax = if self.config.is_racf or false then null else python-super.jax;
      jaxlib = if self.config.is_racf or false then null else python-super.jaxlib;
    };
    python27 = super.python27.override { packageOverrides = self.pythonOverrides; };
    python27Packages = self.python27.pkgs;
    python36 = super.python36.override { packageOverrides = self.pythonOverrides; };
    python36Packages = self.python36.pkgs;
    python37 = super.python37.override { packageOverrides = self.pythonOverrides; };
    python37Packages = self.python37.pkgs;
    python38 = super.python38.override { packageOverrides = self.pythonOverrides; };
    python38Packages = self.python38.pkgs;
    python39 = super.python39.override { packageOverrides = self.pythonOverrides; };
    python39Packages = self.python39.pkgs;
    python311 = super.python311.override { packageOverrides = self.pythonOverrides; };
    python311Packages = self.python311.pkgs;

    snakemake_6_5_0 = self.callPackage pkgs/snakemake/default.nix { };
    htcondor = self.callPackage pkgs/htcondor/default.nix { python = self.python3; };
    twemproxy = self.callPackage pkgs/twemproxy/default.nix { };

    nix = super.nix.overrideAttrs (old: {
      patches = (old.patches or []) ++ [
        (self.fetchpatch {
          url = "https://github.com/NixOS/nix/commit/5c516a10d56ac6d02b8d9a9ad1fe7311ecc59128.diff";
          sha256 = "1a4hqnnhm50ds6pkd6s6x75pdkq19vrjyy817zr0ldvl5cq7a5ls";
        })
        # use poll()
        (self.fetchpatch {
          url = "https://github.com/NixOS/nix/commit/c0d940978a66841df638038e3ca85501bcee5734.diff";
          sha256 = "1p9py1yhi8d6qxkbnvjy0qr36q3c81kqir93ss4hrnjybskjp19n";
        })
        # https://github.com/NixOS/nix/pull/5475
        (self.fetchpatch {
          url = "https://github.com/NixOS/nix/commit/1261a1689bd921b40682bb6720177c2fe23242f9.diff";
          sha256 = "1p5a9d8f7mj9sq26lgyd3k8x6k65b2wbr3x38yr7qjdrwm879a22";
        })
        ./lock.patch
        ./sqlite3_soft_heap_limit64.patch
        ./bzCompressInit-1.patch
      ];
      doInstallCheck = false;
    });
    nixUnstable = overrideAttrsOnRACF (overrideOnRACF super.nixUnstable { mdbook = null; }) (old: {
      configureFlags = (old.configureFlags or []) ++ [ "--disable-doc-gen" ];
      outputs = [ "out" "dev" ];
      meta.outputsToInstall = [ "out" ];
      doInstallCheck = false;
    });

    # 20.09
    xxHash = overrideAttrsOnRACF super.xxHash (_: self.lib.optionalAttrs (super.xxHash.version == "0.8.0") {
      postPatch = ''
        substituteInPlace Makefile \
          --replace '$(error configured libdir' "#" \
          --replace '$(error configured includedir' "#" \
      '';
    });

    findutils = overrideAttrsOnRACF super.findutils (_: { doCheck = false; });
    libuv = overrideAttrsOnRACF super.libuv (_: { doCheck = false; });
    p11-kit = overrideAttrsOnRACF super.p11-kit (_: { doCheck = false; });
    gnutls = overrideAttrsOnRACF super.gnutls (_: { doCheck = false; });
    ghostscript = overrideOnRACF super.ghostscript { cupsSupport = false; x11Support = false; };
    ghostscriptX = if self.config.is_racf or false then self.ghostscript else super.ghostscriptX;
    git = overrideAttrsOnRACF (overrideOnRACF super.git { withManual = false; perlSupport = true; }) (_: { doInstallCheck = false; });
    gitFull = if self.config.is_racf or false then throw "gitFull guard" else super.gitFull;
    procps = overrideOnRACF super.procps { withSystemd = false; };
    cups = if self.config.is_racf or false then null else super.cups;
    go_bootstrap = overrideAttrsOnRACF super.go_bootstrap (old: {
      preConfigure = ''
        substituteInPlace src/run.bash --replace "ulimit" "echo"
        sed -i src/os/os_unix_test.go -e '/func Test/areturn'
      '' + (old.preConfigure or "");
    });
    # runs over limits
    go_1_15 = overrideAttrsOnRACF super.go_1_15 (old: {
      doCheck = false;
      preConfigure = ''
        sed -i src/os/os_unix_test.go -e '/func Test/areturn'
      '' + (old.preConfigure or "");
    });
    go_1_14 = overrideAttrsOnRACF super.go_1_14 (old: {
      doCheck = false;
      preConfigure = ''
        sed -i src/os/os_unix_test.go -e '/func Test/areturn'
      '' + (old.preConfigure or "");
    });
    go_1_13 = overrideAttrsOnRACF super.go_1_13 (old: {
      doCheck = false;
      preConfigure = ''
        sed -i src/os/os_unix_test.go -e '/func Test/areturn'
      '' + (old.preConfigure or "");
    });
    go_1_12 = overrideAttrsOnRACF super.go_1_12 (old: {
      doCheck = false;
      preConfigure = ''
        sed -i src/os/os_unix_test.go -e '/func Test/areturn'
      '' + (old.preConfigure or "");
    });
    qemu = overrideOnRACF super.qemu {
      numaSupport = false;
      seccompSupport = false;
      pulseSupport = false;
      sdlSupport = false;
      gtkSupport = false;
      vncSupport = false;
      smartcardSupport = false;
      spiceSupport = false;
      usbredirSupport = false;
      xenSupport = false;
      cephSupport = false;
      openGLSupport = false;
      virglSupport = false;
      smbdSupport = false;
      tpmSupport = false;
    };

    # apply starting with 20.09
    openssh = overrideOnRACF super.openssh (self.lib.optionalAttrs (self.lib.versionAtLeast super.openssh.version "8") { withFIDO = false; });
    # for 19.09, can use enableSystemd for newer
    libusb1 = if self.config.is_racf or false then null else super.libusb1;
    pcsclite = if self.config.is_racf or false then null else super.pcsclite;
    gnupg = overrideOnRACF super.gnupg (with self.lib; optionalAttrs (versionAtLeast trivial.release "23.11") { withPcsc = false; });
    lvm2 = overrideAttrsOnRACF (overrideOnRACF super.lvm2 { udev = null; }) (old: {
      preConfigure = ''
        sed -i man/Makefile.in -e 's/-e "s+\([^+]\+\)+\([^+]\+\)+"/-e "s|\1|\2|"/'
      '' + (old.preConfigure or "");
    });
    systemd = if self.config.is_racf or false then null else super.systemd;
    systemdMinimal = if self.config.is_racf or false then null else super.systemdMinimal;
    systemdLibs = if self.config.is_racf or false then null else super.systemdLibs;
    dbus = overrideOnRACF super.dbus (with self.lib; optionalAttrs (versionAtLeast trivial.release "23.11") { enableSystemd = false; });
    util-linux = overrideOnRACF super.util-linux (with self.lib; optionalAttrs (versionAtLeast trivial.release "23.11") { systemdSupport = false; });
    xrootd = overrideOnRACF super.xrootd (with self.lib; optionalAttrs (versionAtLeast trivial.release "23.11") { systemd = null; });
    pandoc = if self.config.is_racf or false then null else super.pandoc;
    rdma-core = overrideAttrsOnRACF super.rdma-core (prev: {
      cmakeFlags = prev.cmakeFlags ++ [ "-DNO_MAN_PAGES=1" ]; # disable pandoc
    });
    wayland = if self.config.is_racf or false then throw "wayland" else super.wayland;
    wayland-scanner = if self.config.is_racf or false then null else super.wayland-scanner;
    wayland-protocols = if self.config.is_racf or false then null else super.wayland-protocols;
    graphviz = overrideOnRACF super.graphviz (with self.lib; if (versionAtLeast trivial.release "23.11") then { withXorg = false; } else { libdevil = null; xorg = null; pango = null; });
    texlive = overrideOnRACF super.texlive (with self.lib; optionalAttrs (versionAtLeast trivial.release "23.11") { jdk = null; });

    # 19.09 #71075
    expat = overrideAttrsOnRACF super.expat (_: { patches = []; });

    # 19.09
    rivet = if (super.rivet.version == "2.7.2") && (builtins.length super.rivet.patches == 1) then
      super.rivet.overrideAttrs (old: {
        patches = old.patches ++ [
          (self.fetchpatch {
            url = "https://gitlab.com/hepcedar/rivet/commit/37bd34f52cce66946ebb311a8fe61bfc5f69cc00.diff";
            sha256 = "0wj3ilpfq2gpc33bj3800l9vyvc9lrrlj1x9ss5qki0yiqd8i2aa";
          })
        ];
      })
    # 3.1.4 is an optimistic estimate
    else if self.config.is_racf && (self.lib.versionOlder super.rivet.version "3.1.4") then
      super.rivet.overrideAttrs (old: {
        patches = old.patches ++ [
          # gs 9.52 opacity fix
          (self.fetchpatch {
            url = "https://gitlab.com/hepcedar/rivet/-/commit/25c4bee19882fc56407b0a438f86e1a11753d5e6.diff";
            sha256 = "18p2wk54r0qfq6l27z6805zq1z5jhk5sbxbjixgibzq8prj1a78v";
          })

          # fix https://gitlab.com/hepcedar/rivet/-/issues/200
          (self.fetchpatch {
            url = "https://gitlab.com/hepcedar/rivet/commit/442dbd17dcb3bd6e30b26e54c50f6a8237f966f9.diff";
            includes = [ "bin/make-pgfplots" "bin/make-plots" "bin/make-plots-fast" ];
            sha256 = "0c3rysgcib49km1zdpgsdai3xi4s6ijqgxp4whn04mrh3qf4bmr3";
          })

          # https://gitlab.com/hepcedar/rivet/-/merge_requests/242
          (self.fetchpatch {
            url = "https://gitlab.com/veprbl/rivet/commit/66adce1243e988a9cb409dc0f8f2115ca78392e3.diff";
            sha256 = "0n039r3gfn8fcpjbs0q0il1ixnnm0jh6y9sgv598g5pyd9a05c51";
          })

          # https://gitlab.com/hepcedar/rivet/-/merge_requests/246
          # make-plots: fix wrong logic in Plot.set_xmax()
          (self.fetchpatch {
            url = "https://gitlab.com/hepcedar/rivet/commit/d371c6c10cf67a41c0e4e27c16ff5723d6276ad2.diff";
            sha256 = "0w622rd5darj7qafbbc84blznvy5rnhsdyr2n1i1fkz19mrf5h2p";
          })

          # https://gitlab.com/hepcedar/rivet/-/merge_requests/247
          ./pr247.patch
        ];
        preInstall = (old.preInstall or "") + ''
          substituteInPlace bin/make-plots \
            --replace '"gs"' '"${self.ghostscript}/bin/gs"'
        '';
      })
    else
      super.rivet;

    # 1.8.3 is an optimistic estimate
    yoda =
      let
        extra_patches =
          self.lib.optionals (self.lib.versionOlder super.yoda.version "1.8.3") [
            (self.fetchpatch {
              url = "https://gitlab.com/veprbl/yoda/-/commit/6ee51044efd88d2f6126b3627d921cfb91d94b09.diff";
              sha256 = "1fchnnb6553d6v56v4az01xxiay7k33hxcqpngldp3jc4s1f8yh8";
            })
          ]
          ++
          self.lib.optionals (self.lib.versionOlder super.yoda.version "1.8.4") [
            # for landing of https://gitlab.com/hepcedar/yoda/-/merge_requests/34
            (self.fetchpatch {
              url = "https://gitlab.com/veprbl/yoda/-/commit/7126829a8b59d8b5fe36b73592ecea801fa136eb.diff";
              sha256 = "sha256-SkDAOCw0OLLiRKwV9qbOmRgIIiyEC/HJichOUDUD/fI=";
            })
            # https://gitlab.com/hepcedar/yoda/-/merge_requests/35
            (self.fetchpatch {
              url = "https://gitlab.com/hepcedar/yoda/-/commit/ad4b2fc7346dbcfd991bbf8680ad2101a90178d9.diff";
              sha256 = "1pykdv60nay5rmmvp878g16jxgcjdrd2dsdwrfd1m9cq96l77c0y";
            })
          ]
          ++
          # 1.9.1 is an optimistic estimate
          self.lib.optionals (self.lib.versionOlder super.yoda.version "1.9.1") [
            (self.fetchpatch {
              url = "https://gitlab.com/hepcedar/yoda/-/commit/a2999d78cb3d9ed874f367bad375dc39a1a11148.diff";
              sha256 = "0379z7k3gcy2g9abwyqc11gzvf71h2zf53abl38jpd0mxsdrbih6";
            })
          ];
      in
        super.yoda.overrideAttrs (old: self.lib.optionalAttrs (extra_patches != []) {
          patches = (old.patches or []) ++ extra_patches;
        });

    # 20.09
    # #114270
    graphicsmagick = overrideAttrsOnRACF super.graphicsmagick (old: {
      configureFlags = (old.configureFlags or []) ++ [ "--with-frozenpaths" ];
    } // self.lib.optionalAttrs (self.lib.trivial.release == "21.11") {
      # 21.11
      # Regression from https://github.com/NixOS/nixpkgs/pull/125361
      postConfigure = ":";
    });

    # 20.09
    sherpa = with self.lib; overrideAttrsOnRACF super.sherpa (old: optionalAttrs (versionOlder trivial.release "21.05") {
      configureFlags = (old.configureFlags or []) ++ [ "--enable-pythia" ];
    });

    cmakeNoPatches = super.cmake.overrideAttrs (prev: {
      patches = [];
      preConfigure = builtins.replaceStrings [ "fixCmakeFiles ." ] [ "" ] prev.preConfigure;
    });

    # 20.09
    poppler = overrideAttrsOnRACF super.poppler (prev: {
      __patch_trick = (self.fetchpatch {
        name = "texlive-poppler-0.86.patch";
        url = "https://raw.githubusercontent.com/archlinux/svntogit-packages/1d5ab3404f98785582ffb3b7dcd9c63ca19517bc/texlive-bin/repos/extra-x86_64/texlive-poppler-0.86.patch";
        sha256 = "0pdvhaqc3zgz7hp0x3a4qs0nh26fkvgmr6w1cjljqhp1nyiw2f1l";
      });
    });

    # 20.09 debian patches gone
    libxml2 = overrideAttrsOnRACF super.libxml2 (prev: self.lib.optionalAttrs (self.lib.trivial.release == "20.09") {
      patches = builtins.filter (p: (builtins.match "^CVE.*" (p.name or "")) == null) prev.patches;
    });

    # 21.11
    # https://github.com/NixOS/nixpkgs/pull/156944
    tbb = overrideAttrsOnRACF super.tbb (prev: self.lib.optionalAttrs (self.lib.trivial.release == "21.11") {
      postInstall = ":";
    });

    # 21.11
    openssl = overrideAttrsOnRACF super.openssl (prev: self.lib.optionalAttrs (prev.version == "8.8p1") {
      doCheck = false;
    });

    redis = overrideOnRACF super.redis (prev: {
      withSystemd = false;
    });

    thrift = overrideAttrsOnRACF super.thrift (prev: self.lib.optionalAttrs (prev.version == "0.13.0") {
      doCheck = false;
    });

    ffmpeg_4 = overrideOnRACF super.ffmpeg_4 {
      ffmpegVariant = "headless";
    };
    ffmpeg_5 = overrideOnRACF super.ffmpeg_5 {
      ffmpegVariant = "headless";
    };
    ffmpeg_6 = overrideOnRACF super.ffmpeg_6 {
      ffmpegVariant = "headless";
    };

  }
