{ lib, stdenv, fetchFromGitHub, boost, cmake, curl, libtool, libuuid, log4cpp, munge, openssl, pcre2, perl, python, sqlite }:

let
  _boost = boost.override { enablePython = true; python = python; };
  boost_python_suffix = builtins.replaceStrings [ "." ] [ "" ] python.pythonVersion;
in

stdenv.mkDerivation rec {
  name = "htcondor-${version}";
  version = "10.0.7";

  src = fetchFromGitHub {
    owner = "htcondor";
    repo = "htcondor";
    rev = "V${builtins.replaceStrings [ "." ] [ "_" ] version}";
    sha256 = "0g7l6zlmpsnqqjgismsg7d03f5ax6r5x8zl8s8cf4k55xw4p2zkj";
  };

  nativeBuildInputs = [ cmake perl libtool ];
  buildInputs = [ _boost curl libuuid log4cpp munge openssl pcre2 python python.pkgs.setuptools sqlite ];

  preConfigure = lib.optionalString python.pkgs.isPy3k ''
    substituteInPlace externals/bundles/boost/1.66.0/CMakeLists.txt \
      --replace 'set (BOOST_COMPONENTS python ''${BOOST_COMPONENTS})' \
                'set (BOOST_COMPONENTS python${boost_python_suffix} ''${BOOST_COMPONENTS})'

    sed -i src/condor_starter.V6.1/CMakeLists.txt -e '/exit_37/d'
  '';

  cmakeFlags = [
    "-DSYSTEM_NAME=nixpkgs"
    "-DPROPER=ON" # avoid vendored deps
    "-DPYTHON_BOOST_LIB=boost_python${boost_python_suffix}"
    "-DPYTHON3_BOOST_LIB=boost_python${boost_python_suffix}"
    "-DNO_PHONE_HOME=ON"
    "-DWANT_FULL_DEPLOYMENT=OFF"
    "-DWITH_CREAM=OFF"
    "-DWITH_GLOBUS=OFF"
    "-DWITH_KRB5=OFF"
    "-DWITH_SCITOKENS=OFF"
    "-DWITH_VOMS=OFF"
    "-DCMAKE_SKIP_RPATH=ON"
    "-DWITH_PYTHON_BINDINGS=ON"
  ];

  # ImportError: libclassad.so.10: cannot open shared object file: No such file or directory
  doCheck = false;

  postInstall = ''
    mkdir -p "$(dirname "$out/${python.sitePackages}")"
    ln -sv "$out/lib/python3" "$out/${python.sitePackages}"
  '';

  meta = with lib; {
    description = "An open-source high-throughput computing software framework for coarse-grained distributed parallelization of computationally intensive tasks";
    homepage = https://research.cs.wisc.edu/htcondor/;
    license = licenses.asl20;
    maintainers = with maintainers; [ veprbl ];
    platforms = platforms.all;
  };
}
